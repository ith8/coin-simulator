# Coin Game Simulator
![](sample.gif)

Source code in [coins.hs](coins.hs). See [Multiple Games Simulation](README_multiple-games-simulation.md) for instructions to compute the expected iteration count over multiple runs. 

## Installation
- Prerequisite: [Haskell Platform](https://www.haskell.org/downloads#platform), mainly [GHC](https://www.haskell.org/ghc/) >= 8.6.3

1. Clone:
```bash
$ git clone https://gitlab.com/foo-san/coin-simulator.git
$ cd coin-simulator
```
2. Method A: Compile and Execute 
```bash
$ ghc -O2 -dynamic coins.hs
$ ./coins
```
2. Method B: Load from Interpreter
```bash
$ ghci
Prelude> :load coins.hs
Prelude Main> main
```
## Program Structure

There're three function in our game:
 - `winner` selects a random lucky player each turn
 - `update` updates the game state 
 - `game` displays the game state after each turn
### 0. Set up:
Each game start out with n = N+1 players all has c = C coins. The default is set to 5 players, each with 6 coins. Modify these values to run the simulation for different n and c.
```haskell
4| c :: Int
5| c = 6
6| n :: Int
7| n = 5
```
The game state is represented as a list of n integers. Each players are represented by their position (0-based index) in a list of n = N+1 integers. The number at each index i show how rich player i is. 
```haskell
8| type State = [Int]
```

The game starts out with a list of size n, each element has the same amount c. Default is `replicate 5 6` => [6,6,6,6,6]. The number of turns it takes to get to this initial state is `0`.
```haskell
32| main :: IO Int
33| main = do game (replicate n c) 0
```

### 1. Select a random winner:
`winner` takes a state and returns the index of a lucky player.
```haskell
10| winner :: State -> IO Int
```
1) First, `ps` is the list of all indices of PlayerS with at least 1 coin:
```haskell
11| winner st = do let ps = [i | (c, i) <- zip st [0..], c /= 0]
```
![](ps.png)

For example, if st = [2,0,0,9,8], ps = [0,3,4] since player 1 and 2 are out.

2) Then, `randomRIO` generates a random number between 0 and the maximum index of `ps`. In our example, the range would be (0,2). 
Suppose x = 1; The lucky player would be `ps !! 1` => 3 = i, this person currently holds 9 coins.
```haskell
12|	       x <- randomRIO (0, length ps - 1)
13|	       return (ps !! x)
```
### 2. Updating the game state:
`update` takes a state and the index of a lucky player, `win`, and returns the next state. 
```haskell
15| update :: State -> Int -> State
16| update st win = keep (donate st) win
```
1) First, a transient state, `donate st`, is created where all player with at least 1 coin each donates away 1 coin.
```haskell
18|     donate = map (\c -> if c == 0 then c else c - 1) 
```
`map` takes a function and list and applies that function to each element of that list. 
The lambda expression `\c -> if c == 0 then c else c - 1` takes an amount of coin c. It takes away 1 coin from players with at least 1 coins while left the amount the same for those with 0 coins. Using the previous example, st = [2,0,0,9,8], the transient state after everyone donated is st' = [1,0,0,8,7]

2) The `pile` contains the total amount of coins donated. This is equal to the amount of players with at least 1 coins, which is 3 in our case.
```haskell
19|     pile = length (filter (/= 0) st)
```
3) Finally, a new state is created where our winner gets to keep the pile:
```haskell
20|     keep st' win = take win st' ++ [(st' !! win) + pile] ++ drop (win + 1) st'
```
`take` and `drop` takes or drop the first k or k+1 element of a list, respectively. In our case that k is `win` = 3. The list `keep` returns is `[1,0,0] ++ [8 + 3] ++ [7]` => [1,0,0,11,7]
### 3. The Game:
`game` takes the current state and iteration and simulates a sequence of future states. It finally returns the total length of the game.
```haskell
22| game :: State -> Int -> IO Int
```
The game ends when exactly 1 person has a non-zero amount of coins:
```haskell
27|		  if length (filter (/= 0) st) == 1 then
28|		     return iter
```
Else, we select a random winner, update the game state with that winner. Then, add 1 to the iteration count and rerun `game`.
```haskell
29|		  else do win <- winner st
30|			  game (update st win) (iter + 1)
```
