import System.Random
import Control.Concurrent

c :: Int
c = 6
n :: Int
n = 5
type State = [Int]

winner :: State -> IO Int
winner st = do let ps = [i | (c, i) <- zip st [0..], c /= 0]
	       x <- randomRIO (0, length ps - 1)
	       return (ps !! x)

update :: State -> Int -> State
update st win = keep (donate st) win
  where 
    donate = map (\c -> if c == 0 then c else c - 1)
    pile = length (filter (/= 0) st)
    keep st' win = take win st' ++ [(st' !! win) + pile] ++ drop (win + 1) st'

game :: State -> Int -> IO Int
game st iter = do threadDelay 500000
		  putStr "\ESC[2J\ESC[1;1H"
		  putStrLn ("Iteration: " ++ (show iter))
		  putStrLn (show st)
		  if length (filter (/= 0) st) == 1 then
		     return iter
		  else do win <- winner st
			  game (update st win) (iter + 1)

main :: IO Int
main = do game (replicate n c) 0