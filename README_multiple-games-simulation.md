# Multiple Games Simulation

## Installation
- Prerequisite: [Haskell Platform](https://www.haskell.org/downloads#platform) 

1. Clone:
```bash
$ git clone https://gitlab.com/foo-san/coin-simulator.git
```
2. Compile and Execute:
```bash
$ cd coin-simulator
$ ghc -O2 -dynamic multiple-games-simulation.hs
$ ./multiple-games-simulation
```

## Program Structure
0. Parameters
`n` and `c` are the number of players and coins. By default, this program simulates E(n,c) for all possible combination of n and c up to 10. `r` is the number of repetition for each n and c. By default this is 5000.
```haskell
4| n = 10
5| c = 10
6| r = 5000 
```

There are two addition to [multiple-games-simulation.hs](multiple-games-simulation.hs):

1. `average_iter` takes and n' and c', run the `game` for the n = n' and c = c' a total of r times and stores all r different IterationS count in the list `is`.

`sequence` takes a list of functions and returns a list of results for each function. Here that list is just the `game` function repeated r times, `_ <- [1..r]` serves as a counter.
```haskell
29| average_iter :: Int -> Int -> IO Float
30| average_iter n' c' = do is <- sequence [game (replicate n' c') 0 | _ <- [1..r]]
```
Then we compute the average lengths of each game by first summing all the iteration counts `sum is` and divide that by r:

```haskell
31|		        let ave = fromIntegral (sum is) / fromIntegral r
34|		        return ave
```
 2. Our new main function performs `average_iter` for every pair of n' and c' in {(1,1),(1,2),...(1,c)...(n, c-1),(n,c)}.

```haskell
26| main :: IO [Float]
27| main = sequence [average_iter n' c' | n' <- [1..c], c' <- [1..n]] 
```

### More specific n and c combinations.

To change the range of n and c to (x,y) and (z,w), modify line 27 like so:

```haskell
27 | main = sequence [average_iter n' c' | n' <- [x..y], c' <- [z..w]] 
```

To simulate only for pairs of n and c where c >= n: 

```haskell
27 | main = sequence [average_iter n' c' | n' <- [1..n], c' <- [1..c], c' >= n'] 
```

To simulate for every other n and c:

```haskell
27 | main = sequence [average_iter (2*n') (2*c') | n' <- [1..n], c' <- [1..c]] 
```
## Sample runs

Results from sample runs could be found [here](data.dat).

3D graph of this dataset with n on the x-axis and c on the y-axis. Generated with [gnu plot](http://www.gnuplot.info/).

```bash
$ gunplot
gnuplot> splot "data.dat"
```

![](graph.png)

2D graph: nc vs E(n,c)

![](graph2.png)
