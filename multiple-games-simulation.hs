import System.Random

type State = [Int]
n = 10
c = 10
r = 5000

winner :: State -> IO Int
winner st = do let ps = [i | (c, i) <- zip st [0..], c /= 0]
	       x <- randomRIO (0, length ps - 1)
	       return (ps !! x)

update :: State -> Int -> State
update st win = keep (donate st) win
  where 
    donate = map (\c -> if c == 0 then c else c - 1)
    pile = length (filter (/= 0) st)
    keep st win = take win st ++ [(st !! win) + pile] ++ drop (win + 1) st

game :: State -> Int -> IO Int
game st iter = do if length (filter (/= 0) st) == 1 then
		     return iter
		  else do win <- winner st
			  game (update st win) (iter + 1)

main :: IO [Float]
main = sequence [average_iter n' c' | n' <- [1..c], c' <- [1..n]] 

average_iter :: Int -> Int -> IO Float
average_iter n' c' = do is <- sequence [game (replicate n' c') 0 | _ <- [1..r]]
		        let ave = fromIntegral (sum is) / fromIntegral r
		        putStr ((show n') ++ "\t" ++ (show c') ++ "\t")
		        putStrLn (show ave)
		        return ave
